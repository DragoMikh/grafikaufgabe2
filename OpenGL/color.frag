/*
Tobias Braun
06.05.2021
*/
#version 420 core

uniform float cIntesity;

in vec4 col;
layout (location = 0) out vec4 fColor;

void main(void)
{
	//Set Color
	fColor = col * cIntesity;
}