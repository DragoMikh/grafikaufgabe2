/*
Tobias Braun
06.05.2021
*/
#version 420 core

layout (location = 0) in vec3 vPosition;
layout (location = 1) in vec4 vColor;
out vec4 col;

void main(void)
{
	gl_Position = vec4(vPosition,0);
	//Hand over Color to Fragment Shader
	col = vColor;
}